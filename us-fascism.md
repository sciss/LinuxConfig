Since the USA is now turning into a fascist country and enemy of liberal democracies, we can take care not to invest a single $ in its economy.

Disclaimar: This list might contain errors. It is solely intended for my own consultation, and is only checked into a public git repository for convenience.

Here are audio equipment manifacturers based in USA:

- Akai Professional
- AKG (\*)
- Alesis
- API - Automated Processes Inc
- Apogee
- ART - Applied Research and Technology
- Auralex Acoustics
- Avalon Design
- Avid
- BAE Audio
- Black Lion Audio
- Bricasti
- Burl Audio
- Chandler Limited
- Crane Song
- Crown Audio (\*)
- dbx (\*)
- Dangerous Music
- Digidesign
- Electro-Harmonix
- Elgato
- Empirical Labs
- EV - Electro-Voice
- Fender
- Fishman
- FMR Audio
- Furman
- Gator
- Gibson
- Golden Age Audio
- Great River
- Harrison Audio (but subsidiary of SSL)
- JBL (\*)
- Kurzweil (ownership KR)
- Line 6
- MÄAG
- Mackie
- Manley
- M-Audio
- Metric Halo
- Meyer Sound
- Millennia Media
- Moog
- MOTU - Mark of the Unicorn
- PreSonus
- QSC
- Rane
- RND - Rupert Neve Designs
- Rolls
- SanDisk
- sE Electronics (?)
- Seagate
- Sequential
- Shure
- SKB
- Sound Devices
- Tascam (division of TEAC, JP)
- TP-Link (? ; and SG)
- Universal Audio
- Waves
- Western Digital

The companies are not based in USA, nor in Russia or China:

- ADAM - Advanced Dynamic Audio Monitors (DE)
- Adam Hall (DE)
- Adelsklang (DE)
- Allen & Heath (UK)
- AMS Neve (UK)
- Arturia (FR)
- Asparion (DE)
- ATC (UK)
- Audient (UK)
- Audio-Technica (JP)
- Austrian Audio (AT)
- AV Stumpfl (AT)
- Bettermaker (PL)
- Beyerdynamic (DE)
- Blackmagic Design (AU)
- Boss (JP)
- Buso Audio (NL)
- Buzz Audio (NZ)
- Clavia Digital Musical Instruments (SE)
- Cordial (DE)
- Cranborne (UK)
- DAV Electronics (UK)
- d\&b (DE)
- Defender -&gt; Adam Hall (DE)
- DPA - Danish Professional Audio (DK)
- Drawmer (UK)
- Dynacord (DE)
- Dynaudio (DK)
- Edutige (KR)
- Elektron Music Machines (SE)
- Erica Synths (LV)
- Eurolite (DE)
- Faderfox (DE)
- Ferrofish (DE)
- Flock Audio (CA)
- Flyht Pro -&gt; Thomann (DE; made in PRC?)
- Focusrite (UK)
- Fredenstein (__TW__)
- Elysia (DE)
- EQ Acoustics (UK)
- Focal (FR)
- Fostex (JP)
- Genelec (FI)
- Gerband (DE)
- Ghielmetti (CH)
- Glorious (DE)
- Goly (DK)
- Gravity -&gt; Adam Hall (DE)
- Gyraf (DK)
- HCL - Handcrafted Labs (__UA__; offices in PL and TH)
- Heritage Audio (ES)
- HK Audio (DE)
- HOFA (DE)
- Ibanez (JP)
- IGS Audio (PL)
- IMG Stageline -&gt; Monacor (DE)
- Jomox (DE)
- K&M - König \& Meyer (DE)
- Korg (JP)
- Lake People (DE)
- Lee Filters (UK)
- Lewitt (AT)
- Line Audio (SE)
- LogiLink (DE?)
- Looptrotter (PL)
- Manfrotto (IT)
- ME Geithain (DE)
- Microtech Gefell (DE)
- Mutec (DE)
- Neumann (DE)
- Neutrik (LI)
- Novation (UK)
- Omnitronic (DE)
- Palmer -&gt; Adam Hall (DE)
- PSI Audio (CH)
- Radial Engineering (CA)
- RCF (IT)
- Rean -&gt; Neutrik (LI)
- RME (DE)
- Rockbag (DE)
- Røde (AU)
- Roland (JP)
- Rosco (UK; USA?)
- Rycote (UK)
- Samsung (KR)
- Sanken (JP)
- Schertler (CH)
- Schoeps (DE)
- Sennheiser (DE)
- Sessiondesk (DE)
- Sitka Instruments (__UA__)
- Sonible (AT)
- Sony (JP)
- Soundman (DE)
- SPL - Sound Performance Lab (DE)
- SSL - Solid Static Logic (UK)
- Swissonic -&gt; Thomann (DE)
- Tannoy (UK)
- Teenage Engineering (SE)
- Tegeler (DE)
- Thermionic Culture (UK)
- TK Audio (SE)
- Tube-Tech (DK)
- Vermona (DE)
- Vicoustic (PT)
- Visaton (DE)
- Waldorf (DE)
- Warm Audio (PL)
- Wavebone (__TW__)
- WesAudio (PL)
- Yamaha (JP)
- Zaor (RO, LU)
- Zoom (JP)

These brands essentially belong to Behringer / Music Tribe:

- Behringer (DE)
- Klark Teknik (PH)
- Midas (UK)
- TC Electronic (DK)

Chinese brands:

- ICON Pro Audio

Russian brands:

- Oktava

Other problematic countries:

- Gainlab (HU)

Caveat: Many companies are essentially multi-national (e.g. Wolfmix - parent company CH, but offices in USA etc.), and many are manifacturing in PRC. E.g. (\*) Harman (USA) is a subsidiary of Samsung (KR). Soundcraft (UK) is a subsidiary of Harman, etc.

