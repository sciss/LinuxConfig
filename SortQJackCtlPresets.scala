// import de.sciss.file._
val confFile = userHome / ".config" / "rncbc.org" / "QjackCtl.conf"
require (confFile.isFile)
val contents = io.Source.fromFile(confFile, "UTF-8").getLines.toList
val i = contents.indexOf("[Presets]") + 1
require (i > 0)
val j = contents.indexWhere(_.startsWith("["), i) - 1
require (j >= i)
val pre  = contents.take(i)
val post = contents.drop(j)
val (change, keep) = contents.slice(i, j).partition(_.startsWith("Preset"))
val names = change.map(s => s.drop(s.indexOf('=') + 1)).sortWith({ case (a, b) => File.NameOrdering.lt(file(a.toUpperCase), file(b.toUpperCase))})
val pairs = names.zipWithIndex.map({ case (n, i) => s"Preset${i + 1}=$n" })
val contentsOut = pre ++ keep ++ pairs ++ post
contentsOut.foreach(println)
val bytesOut = contentsOut.mkString("\n").getBytes("UTF-8")
val fOut = new java.io.FileOutputStream(confFile); fOut.write(bytesOut); fOut.close()
