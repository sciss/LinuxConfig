#!/bin/bash
if [ $# -eq 0 ]
  then
	  echo "No arguments supplied. Need: input.pdf (output.pdf)"
    exit 1
fi

INPUT=$1

if [ -z "$2" ]
  then
	  # OUT_NAME=$(basename -- "$1")
          # OUT_BASE="${OUT_NAME%.*}"
          OUT_BASE="${1%.*}"
	  OUTPUT="${OUT_BASE}_web.pdf"
  else
	  OUTPUT=$2
fi

if [ ! -f $INPUT ]; then
    echo "Input '$INPUT' not found!"
    exit 1
fi
 
if [ ! -f $OUTPUT ]; then
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dBATCH  -dQUIET -sOutputFile=$OUTPUT $INPUT
    exit 0
else
    echo "Output '$OUTPUT' already exists. Not overwriting!"
    exit 1
fi

