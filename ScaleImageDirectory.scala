val dir = userHome / "Downloads"
val in = dir.children(f => f.name.startsWith("20250") && f.base.lastOption.exists(_.isDigit) && f.extL == "jpg")
// in.size; in.foreach(println)
in.foreach { fIn =>
  val fOut = fIn.parent / s"${fIn.base}fhd.jpg"
  if (!fOut.exists()) {  
    val cmd = Seq("convert", fIn.path, "-resize", "1920x1920", fOut.path)
    import sys.process._
    cmd.!
  }
}
