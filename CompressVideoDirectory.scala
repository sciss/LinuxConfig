val dir = userHome / "Downloads"
val in = dir.children(f => f.name.startsWith("20250") && !f.name.contains("m.mp4") && f.extL == "mp4")
// in.size
in.foreach { fIn =>
  val fOut = fIn.parent / s"${fIn.base}m.mp4"
  require (!fOut.exists())
  val cmd = Seq("ffmpeg", "-i", fIn.path, "-crf", "23", fOut.path)
  import sys.process._
  cmd.!
}
