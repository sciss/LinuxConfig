#!/bin/bash
ID_SINK=$(pactl list short modules | grep module-jack-sink | cut -f 1)
ID_SOURCE=$(pactl list short modules | grep module-jack-source | cut -f 1)
echo $ID_SINK
echo $ID_SOURCE
pactl unload-module $ID_SINK
pactl unload-module $ID_SOURCE
