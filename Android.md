# Synchronising Data

via KDE Connect on the phone and GS Connect on the desktop.

This only works reliably when using Mobile Hotspot *on the phone* (and connecting the desktop there), rather than the house Wifi.
When getting the 'port already in use' error on the desktop:

    $ kill -15 $(lsof -t -i UDP:1716)

See https://github.com/GSConnect/gnome-shell-extension-gsconnect/wiki/Error#port-already-in-use

