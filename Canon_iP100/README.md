# Getting Canon iP100 to work on Debian 12

- unpack the `cnijfilter-ip100series-3.70-1-deb.tar.gz`
- `sudo apt install libpango1.0-0`
- `sudo dpkg -i libpng12-0_1.2.54-6_amd64.deb`
- `sudo dpkg -i libtiff4_3.9.2-2ubuntu0.16_amd64.deb`
- `cnijfilter-ip100series-3.70-1-deb/install.sh`

References:

- https://askubuntu.com/questions/1495476/cannot-install-canon-pixma-ip100-in-ubuntu-22-04
- https://unix.stackexchange.com/questions/761068/installing-libpng12-64-bit-on-debian-12-bookworm-64-bit-gives-a-multiarch-supp
- http://snapshot.debian.org/package/libpng/1.2.54-6/#libpng12-0_1.2.54-6
- https://askubuntu.com/questions/1359381/missing-libtiff4-while-installing-canon-mx410-driver-on-ubuntu-21-04
- https://drive.google.com/drive/folders/1DUKKvui64R5gqVdhxYn-31y8HXmP8lxe

